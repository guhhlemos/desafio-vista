<?php 

    // Testes
    // $vars['imoveis'] = [
    //     [
    //         'Codigo' => '213213771',
    //         'Cidade' => 'FLORIANOPOLIS',
    //         'Bairro' => 'ITACORUBI',
    //         'ValorVenda' => '12580.78',
    //         'ValorLocacao' => '588.41',
    //         'Vagas' => '',
    //         'Churrasqueira' => 'Nao',
    //         'Lareira' => 'Nao',
    //         'Dormitorios' => '4',
    //         'Vagas' => '2',
    //         'AreaTotal' => '50',
    //         'FotoDestaque' => 'http://vistaidc.com.br/sandbox/vista.imobi/fotos/5397/isKJ3MDf4Kn79c_53975c82af24cd47e.jpg'
    //     ]
    // ];
?>

<div class="container-fluid imoveis_template">
    <div class="row">
        <?php if (empty($vars['imoveis'])) { ?>
            Nenhum resultado encontrado
        <?php } ?>
        <?php foreach ($vars['imoveis'] as $imovel) { ?>
            <div class="col-4">
                <div class="row">
                    <div class="col p-0 m-0">
                        <img src="<?php echo $imovel['FotoDestaque'] ?>" alt="">
                    </div>
                </div>
                <div class="row">
                    <div class="col p-0 m-0">
                        <p class="venda p-2 m-0">Venda <span>R$ <?php echo number_format($imovel['ValorVenda'], 2, ',', '.') ?></span></p>
                        <p class="aluguel p-2 m-0">Aluguel <span>R$ <?php echo number_format($imovel['ValorLocacao'], 2, ',', '.') ?></span></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col border p-0 m-0">
                        <span class="p-2 m-0 cidade"><?php echo $imovel['Cidade'] ?></span> | 
                        <span class="p-2 m-0 bairro"><?php echo $imovel['Bairro'] ?></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col border p-2 m-0">
                        <p class="p-0 m-0">
                            <i class="fas fa-bed"></i> Dormitórios 
                            <span><?php echo $imovel['Dormitorios'] ?></span>
                        </p>
                        <p class="p-0 m-0">
                        <i class="fas fa-car"></i> Vagas 
                            <span><?php echo $imovel['Vagas'] ?></span>
                        </p>
                        <p class="p-0 m-0">
                            <i class="fa fa-arrows-alt"></i> Área Total 
                            <span><?php echo $imovel['AreaTotal'] ?>m²</span>
                        </p>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>