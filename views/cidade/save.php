<?php include_once __DIR__."/../../views/layouts/head.php"; ?>

<body>

  <?php include_once __DIR__."/../../views/layouts/nav.php"; ?>

  <div class="container-fluid">

		<?php $route = (isset($vars['cidade'])) ? "{$url}/cidade/{$vars['cidade']->id}" : "{$url}/cidade" ?>

		<h1><?php echo (isset($vars['cidade'])) ? 'Alterar' : 'Cadastrar' ?> Cidade</h1>

		<form action="<?php echo $route ?>" method="post">
			<div class="row">
				<div class="form-group col-12">
					<label>Nome do cidade</label>
					<input id="nome" type="text" name="nome" class="form-control" placeholder="Insira o nome do cidade" maxlength="50" required>
				</div>
				<div class="form-group col-12">
					<button type="submit" class="btn btn-primary">
						Salvar
					</button>
				</div>
			</div>
		</form>
	</div>

	<script>
	$(function () {

		let cidade = null;

		<?php if (isset($vars['cidade'])) { ?>
		cidade = <?php echo ($vars['cidade']); ?>
		<?php } ?>

		console.log(cidade)
		
		if (cidade) {
			$('[name=nome]').val(cidade.nome)
		}
	})
	</script>

</body>
</html>
