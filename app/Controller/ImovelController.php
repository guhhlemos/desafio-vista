<?php

namespace App\Controller;

use App\Model\Cidade;
use App\Model\Bairro;

class ImovelController extends Controller {

  public static function index() {

    $cidades = Cidade::all();

    return self::view('imovel/index', compact('cidades'));
  }
  
  public static function buscar_bairros() {

    $params = $_GET;

    $bairros = Bairro::where('cidade_id', $params['cidade'])->get();

    return json_encode($bairros->toArray());
  }

  public static function consultar_imoveis() {

    $dados = [
      'fields' => [
        'Codigo', 'Cidade', 'Bairro', 'ValorVenda', 'ValorLocacao',
        'Vagas', 'Churrasqueira', 'Lareira', 'FotoDestaque',
        'Dormitorios', 'Vagas', 'AreaTotal',
      ]
    ];

    $params = $_GET;

    if (!key_exists('bairros', $params) || !key_exists('cidade', $params)) {
      http_response_code(400);
      die;
    }

    $cidade = Cidade::findOrFail($params['cidade']);
    $bairros = Bairro::findOrFail($params['bairros']);

    $filter = [
      'Cidade' => [$cidade->nome],
      'Bairro' => array_column($bairros->toArray(), 'nome'),
    ];

    $dados['filter'] = $filter;

    $key = 'c9fdd79584fb8d369a6a579af1a8f681';
    $postFields = json_encode($dados);

    // Remove possíveis espaços entre os campos (como os bairros e cidades) e adiciona '_' (underline)
    $postFields = preg_replace('!\s+!', ' ', $postFields);
    $postFields = str_replace(" ", "_", $postFields);
    
    $url = 'http://sandbox-rest.vistahost.com.br/imoveis/listar?key=' . $key;
    $url .= '&pesquisa=' . $postFields;
    // $url .= '&showtotal=1';

    // Envia a requisição via cURL
    $ch = curl_init($url);
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch, CURLOPT_HTTPHEADER , array( 'Accept: application/json' ) );
    $result = curl_exec( $ch );
    curl_close($ch);

    $result = json_decode( $result, true );

    $imoveis = (key_exists('message', $result) && $result['message'] == "A pesquisa não retornou resultados.") ? [] : $result;

    return self::view('imovel/template', compact('imoveis'));
  }
}
