<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model as Eloquent;
use App\Model\Cidade;

class Bairro extends Eloquent {

  protected $fillable = [
    'nome'
  ];

  public function cidade() {
    return $this->belongsTo(Cidade::class);
  }
}
