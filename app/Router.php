<?php

namespace App;

class Router {
    /* armazenamento das rotas e dos parâmetros atuais do HTTP */
    private $routes = [];
    private static $params = [];
    /* verifica se método chamado é GET, POST ou DELETE */
    private function validate(string $method) {
        return in_array($method, ['get', 'post', 'delete']);
    }
    /*
        Chamado sempre que existe uma chamada nessa classe, caso a chamada
        seja válida, irá separar e validar os argumentos recebidos, devendo receber
        uma string e uma função. E por fim armazena no array de rotas, com a estrutura:
        array(2) {
            ["get"]=> array(3) {
                ["/"]=> object(Closure)#2 (0) { }
                ...
            }
            ["post"]=> array(1) {
                ["/list"]=> object(Closure)#5 (0) { }
                ...
            }
        }
    */
    public function __call(string $method, array $args) {

        $method = strtolower($method);

        if (!$this->validate($method))
            return false;

        [$route, $action] = $args;

        if (!isset($action) or !is_callable($action))
            return false;

        $this->routes[$method][$route] = $action;

        // var_dump($this->routes);
        // die();

        return true;
    }
    /*
        Dá início a aplicação, verificando se existem rotas
        com o método HTTP atual (post, get ou delete), se existe a rota conforme a URI. 
        E por fim chamando o callable da rota correspondente,
        finalizando a aplicação exibindo o seu retorno (a resposta do Controller).
    */
    public function run () {

        $method = strtolower($_SERVER['REQUEST_METHOD']) ?? 'get';
        $route = $_SERVER['REQUEST_URI'];
        $route = substr($route, 1); // remove primeiro char (/)

        // Remove parametros da URI após ? (usado no get)
        if (strstr($route, '?', true)) {
            $route = strstr($route, '?', true);
        }

        if(!isset($this->routes[$method]))
            die('405 Method not allowed');

        $exploded = explode("/", $route);

        $splited_route = [];
        $params = [];

        // Verifica se a rota possui parametros {id} para separar estes parametros e
        // enviar para closure no final do método
        foreach ($exploded as $value) {

            $is_int = filter_var($value, FILTER_VALIDATE_INT);
            
            if ($is_int) {
                array_push($params, $is_int);
                array_push($splited_route, '{id}');
            } else {
                array_push($splited_route, $value);
            }
        }

        $route = join('/', $splited_route);

        if(!isset($this->routes[$method][$route]))
            die('404 Error');

        die( call_user_func_array($this->routes[$method][$route], $params) );
    }
    /*
        Pega as variáveis correspondente ao método atual, sendo os dados
        enviados pelo cliente.
    */
    private function getParams(string $method) {
        if($method == 'get')
            return $_GET;
        return $_POST;
    }
    /*
        Getter para que controlador possa pegar os dados da requisição do cliente
    */
    public static function getRequest() {
        return self::$params;
    }
}
