<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">
        <i class="far fa-building"></i>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">

        <li class="nav-item <?php echo $_SERVER['REQUEST_URI'] == '/imoveis' ? 'active' : '' ?>">
            <a class="nav-link" href="<?php echo "{$url}/imoveis" ?>">Imóveis</a>
        </li>
        <li class="nav-item <?php echo $_SERVER['REQUEST_URI'] == '/cidades' ? 'active' : '' ?>">
            <a class="nav-link" href="<?php echo "{$url}/cidades" ?>">Cidades</a>
        </li>
        <li class="nav-item <?php echo $_SERVER['REQUEST_URI'] == '/bairros' ? 'active' : '' ?>">
            <a class="nav-link" href="<?php echo "{$url}/bairros" ?>">Bairros</a>
        </li>
    </div>
</nav>