<?php

require_once __DIR__ . '/../vendor/autoload.php';

use App\Router;
use Illuminate\Database\Capsule\Manager as Capsule;
use App\Controller\CidadeController;
use App\Controller\BairroController;
use App\Controller\ImovelController;

$capsule = new Capsule;

$capsule->addConnection([
   "driver" => "mysql",
   "host" =>"127.0.0.1",
   "database" => "vista",
   "username" => "root",
   "password" => "server"
]);

//Make this Capsule instance available globally.
$capsule->setAsGlobal();

// Setup the Eloquent ORM.
$capsule->bootEloquent();
$capsule->bootEloquent();

$app = new Router();

// Rota padrão
$app->get('', function () {
    return ImovelController::index();
});

// Cidades
$app->get('cidades', function () {
    return CidadeController::index();
});
$app->get('cidade/{id}', function ($id) {
    return CidadeController::show($id);
});
$app->get('cidade/{id}/bairros', function ($id) {
    return CidadeController::bairros($id);
});
$app->get('cidade/create', function () {
    return CidadeController::create();
});
$app->get('cidade/{id}/edit', function ($id) {
    return CidadeController::edit($id);
});
$app->post('cidade', function () {
    return CidadeController::store();
});
$app->post('cidade/{id}', function ($id) {
    return CidadeController::update($id);
});
$app->delete('cidade/{id}', function ($id) {
    return CidadeController::delete($id);
});

// Bairros
$app->get('bairros', function () {
    return BairroController::index();
});
$app->get('bairro/{id}', function ($id) {
    return BairroController::show($id);
});
$app->get('bairro/create', function () {
    return BairroController::create();
});
$app->get('bairro/{id}/edit', function ($id) {
    return BairroController::edit($id);
});
$app->post('bairro', function () {
    return BairroController::store();
});
$app->post('bairro/{id}', function ($id) {
    return BairroController::update($id);
});
$app->delete('bairro/{id}', function ($id) {
    return BairroController::delete($id);
});

// Imóveis
$app->get('imoveis', function () {
    return ImovelController::index();
});
$app->get('imoveis/buscar_bairros', function () {
    return ImovelController::buscar_bairros();
});
$app->get('imoveis/consultar_imoveis', function () {
    return ImovelController::consultar_imoveis();
});

$app->run();
