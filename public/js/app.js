$(function () {
    $('.excluirRegistro').on('submit', function (event) {

        event.preventDefault()

        if (!confirm("Deseja excluir o registro?")) {
            return
        }

        var form = $(this)

        $.ajax({
            type: $(form).attr('method'),
            url: $(form).attr('action'),
            beforeSend: function () {

            },
            complete: function () {

            },
            success: function (response) {
                // console.log(JSON.parse(response))
                console.log(response)

                $(form).closest('tr').remove()
            },
            error:function (response) {
                console.log(response)
            },
        })
    })
})