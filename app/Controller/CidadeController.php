<?php

namespace App\Controller;

use App\Model\Cidade;

final class CidadeController extends Controller {

  public static function index() {

    $cidades = Cidade::with('bairros')->get();
    
    return self::view('cidade/index', ['cidades' => $cidades]);
  }

  public static function create() {

    return self::view('cidade/save');
  }

  public static function edit($id) {

    $cidade = Cidade::findOrFail($id);
    
    return self::view('cidade/save', compact('cidade'));
  }

  public static function store() {

    $params = $_POST;

    $cidade = new Cidade;
    $cidade->nome = $params['nome'];
    $cidade->save();

    self::redirect('cidades');
  }
  
  public static function update($id) {

    $params = $_POST;

    $cidade = Cidade::findOrFail($id);
    $cidade->nome = $params['nome'];
    $cidade->save();

    self::redirect('cidades');
  }

  public static function delete($id) {

    $cidade = Cidade::findOrFail($id);
    $cidade->delete();
    
    return true;
  }
}
