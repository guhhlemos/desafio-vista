<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model as Eloquent;
use App\Model\Bairro;

class Cidade extends Eloquent {
  
  protected $fillable = [
    'nome'
  ];
  
  public function bairros() {
    return $this->hasMany(Bairro::class);
  }

}
