<?php include_once __DIR__."/../../views/layouts/head.php"; ?>

<body>

  <?php include_once __DIR__."/../../views/layouts/nav.php"; ?>

  <div class="container-fluid">

		<?php $route = (isset($vars['bairro'])) ? "{$url}/bairro/{$vars['bairro']->id}" : "{$url}/bairro" ?>

		<h1><?php echo (isset($vars['bairro'])) ? 'Alterar' : 'Cadastrar' ?> Bairro</h1>

		<form action="<?php echo $route ?>" method="post">
			<div class="row">
				<div class="form-group col-12">
					<label>Nome do bairro</label>
					<input id="nome" type="text" name="nome" class="form-control" placeholder="Insira o nome do bairro" maxlength="50" required>
				</div>
				<div class="form-group col-12">
					<label for="exampleFormControlSelect1">Cidade</label>
					<select class="js-example-basic-multiple-cidades form-control" name="cidade_id">
						<?php foreach ($vars['cidades'] as $cidade) { ?>
						<option value="<?php echo $cidade->id ?>"><?php echo $cidade->nome ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="form-group col-12">
					<button type="submit" class="btn btn-primary">
						Salvar
					</button>
				</div>
			</div>
		</form>
	</div>

	<script>
	$(function () {

		let bairro = null;

		<?php if (isset($vars['bairro'])) { ?>
		bairro = <?php echo ($vars['bairro']); ?>
		<?php } ?>

		console.log(bairro)
		
		if (bairro) {
			$('[name=nome]').val(bairro.nome)
			$('[name=cidade_id]').val(bairro.cidade.id).change()
		}

		$('.js-example-basic-multiple-cidades').select2();
	})
	</script>

</body>
</html>
