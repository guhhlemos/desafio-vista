<?php include_once __DIR__."/../../views/layouts/head.php"; ?>

<body>

  <?php include_once __DIR__."/../../views/layouts/nav.php"; ?>
  
  <div class="container-fluid">

    <h1>Cidades</h1>

    <a class="btn btn-outline-success" href="cidade/create">
      Novo
    </a>

    <table class="table">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Nome</th>
          <th scope="col"></th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($vars['cidades'] as $cidade) { ?>
          <tr>
            <th><?php echo $cidade->id ?></th>
            <td><?php echo $cidade->nome ?></td>
            <td>
              <button type="button" class="btn btn-outline-info verBairros" data-cidade="<?php echo $cidade->id ?>">
                Ver Bairros
              </button>
              <a class="btn btn-outline-primary" href="cidade/<?php echo $cidade->id ?>/edit">
                Alterar
              </a>
              <form class="excluirRegistro d-inline" action="cidade/<?php echo $cidade->id ?>" method="delete">
                <button type="submit" class="btn btn-outline-danger">
                  Excluir
                </button>
              </form>
          </td>
        </tr>
      <?php } ?>
    </tbody>
  </table>

  <?php include_once __DIR__."/../../views/cidade/bairros.php" ?>

</div>

<script>
$(function () {

  let cidades = <?php echo ($vars['cidades']) ?>

  console.log(cidades)

  $('.verBairros').on('click', function (event) {

    let cidade_click = $(this).data('cidade')

    let cidade = cidades.filter(function(c) {
      return c.id == cidade_click;
    });

    cidade = cidade[0]

    let modal_body = $('#verBairrosModal').find('.modal-body')

    $(modal_body).html('')

    $.each(cidade.bairros, function (key, bairro) {
      $(modal_body).append($('<p>', {
        text: bairro.nome,
      }))
    })

    $('#verBairrosModal').modal('show');
  })

})
</script>

</body>
</html>
