<?php

namespace App\Controller;

use App\Model\Bairro;
use App\Model\Cidade;

final class BairroController extends Controller {

  public static function index() {

    $bairros = Bairro::all();
    $cidades = Cidade::all();
    
    return self::view('bairro/index', compact('bairros', 'cidades'));
  }
  
  public static function create() {

    $cidades = Cidade::all();
    
    return self::view('bairro/save', compact('cidades'));
  }

  public static function edit($id) {

    $bairro = Bairro::findOrFail($id);
    $bairro->cidade;
    $cidades = Cidade::all();
    
    return self::view('bairro/save', compact('bairro', 'cidades'));
  }

  public static function store() {

    $params = $_POST;

    $bairro = new Bairro;
    $bairro->nome = $params['nome'];
    $bairro->cidade_id = $params['cidade_id'];
    $bairro->save();

    // return json_encode($bairro->toArray());
    self::redirect('bairros');
  }

  public static function update($id) {

    $params = $_POST;

    $bairro = Bairro::findOrFail($id);
    $bairro->nome = $params['nome'];
    $bairro->cidade_id = $params['cidade_id'];
    $bairro->save();

    // return json_encode($bairro->toArray());
    self::redirect('bairros');
  }

  public static function delete($id) {

    $bairro = Bairro::findOrFail($id);
    $bairro->delete();
    
    return true;
  }
}
