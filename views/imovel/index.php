<?php include_once __DIR__."/../../views/layouts/head.php"; ?>

<body>

	<?php include_once __DIR__."/../../views/layouts/nav.php"; ?>

    <div class="container-fluid">

		<h1>Imóveis</h1>

		<form id="consultarImoveis" action="imoveis/consultar_imoveis" method="get">
			<div class="form">
				<div class="form-group col-6">
					<label for="exampleFormControlSelect1">Escolha a cidade</label>
					<select class="js-example-basic-multiple-cidades form-control" name="cidade">
						<option value="">Escolha</option>
						<?php foreach ($vars['cidades'] as $cidade) { ?>
						<option value="<?php echo $cidade->id ?>"><?php echo $cidade->nome ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="form-group col-6">
					<label for="exampleFormControlSelect1">Escolha os bairros</label>
					<select class="js-example-basic-multiple-bairros form-control" name="bairros[]" multiple="multiple">
					</select>
				</div>
				<div class="form-group col-6">
					<button type="submit" class="btn btn-primary form-control">
						Consultar Imóveis
					</button>
				</div>
			</div>
		</form>

		<div id="imoveis_template"></div>

    </div>

<script>
$(function () {

	$('.js-example-basic-multiple-cidades').select2();
	$('.js-example-basic-multiple-bairros').select2();

	$('.js-example-basic-multiple-cidades').on('change', function () {

		$('.js-example-basic-multiple-bairros').find('option').remove();

		var data = {cidade: $(this).val()}
		
		$.ajax({
            type: 'GET',
            url: 'imoveis/buscar_bairros',
            data: data,
            success: function (response) {
				
				response = JSON.parse(response)

				$.each(response, function (key, bairro) {
					$('.js-example-basic-multiple-bairros').append($('<option>', {
						value: bairro.id,
						text: bairro.nome,
					}))
				})
				
            },
            error:function (response) {
                console.log(response)
            },
		})
		
		
	})
	$('#consultarImoveis').on('submit', function (event) {

		event.preventDefault()

		$('#imoveis_template').html('');

		var form = $(this)
		
		$.ajax({
            type: $(form).attr('method'),
            url: $(form).attr('action'),
            data: $(form).serialize(),
            success: function (response) {
				$('#imoveis_template').html(response);
            },
            error:function (response) {
                console.log(response)
            },
        })
	})

})
</script>

</body>
</html>