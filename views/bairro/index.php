<?php include_once __DIR__."/../../views/layouts/head.php"; ?>

<body>

  <?php include_once __DIR__."/../../views/layouts/nav.php"; ?>

  <div class="container-fluid">

    <h1>Bairros</h1>

    <a class="btn btn-outline-success" href="bairro/create">
      Novo
    </a>

    <table class="table">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Nome</th>
          <th scope="col">Cidade</th>
          <th scope="col"></th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($vars['bairros'] as $bairro) { ?>
          <tr>
            <th><?php echo $bairro->id ?></th>
            <td><?php echo $bairro->nome ?></td>
            <td><?php echo $bairro->cidade->nome ?></td>
            <td>
              <a class="btn btn-outline-primary" href="bairro/<?php echo $bairro->id ?>/edit">
                <!-- <i class="far fa-edit"></i> -->
                Alterar
              </a>
              <form class="excluirRegistro d-inline" action="bairro/<?php echo $bairro->id ?>" method="delete">
                <button type="submit" class="btn btn-outline-danger">
                  Excluir
                </button>
              </form>
            </td>
          </tr>
        <?php } ?>
      </tbody>
    </table>

</body>
</html>
