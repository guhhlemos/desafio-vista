# Desafio - Vista

- Versão PHP utilizada: 7.2
- Banco de dados: MySQL
- Servidor Web: PHP Embutido

## Procedimentos

- Importar o sql disponível em database/vista.sql

Obs.: Na API da Vista encontrei imóveis na cidade de Florianópolis, bairro Itacorubi, portanto esses dados já estão presentes no SQL para facilitar o uso inicial.


- Em public/bootstrap.php é necessário alterar as credenciais para acesso ao banco de dados.


- No diretório principal, basta iniciar o servidor web php:
$ make serve


- Acessar a página:
http://192.168.0.78:8008/



